// Offline
const offlineCache = [
    '/index.html',
    '/projeto-arquitetura.html',
    '/projeto-interiores.html'
];

evt.waitUntil(
    caches.open(offlineCacheFile).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page');
      return cache.addAll(offlineCache);
    })
);